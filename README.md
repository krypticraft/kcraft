# kCraft #

Please keep note that this won't include actual java files as some are licenced to not be re-distributed via any other means but their authors website/original means.
This is simply for CONFIG and WIKI use only.

### What is this repository for? ###

* Keeping track of Config File Edits
* Issue Tracking (crashes)
* Wiki Help-sheet and other useful information.

### How do I get kCraft??? ###

* Via our own launcher, which will be linked here as soon as we have a dedicated website and capable host that will keep up with our throughput demands.

### Contribution Wall ###

* xKryptx - The ModPack Author.

### Who do I talk to? ###

* Contact xKryptx for any information.